//
//  UIView+Functional.swift
//  FunctionalUI
//
//  Created by Dmitry Shelonin on 13/07/2019.
//  Copyright © 2019 Dmitry Shelonin. All rights reserved.
//

import UIKit

//MARK: - VIEW HIERARHY

extension UIView {
    public final func added(to view: UIView) -> Self {
        view.addSubview(self)
        return self
    }

    public final func center(in view: UIView) -> Self {
        view.addSubview(self)
        return self.centered()
    }
}

//MARK: - CONSTRAINTS

extension UIView {
    public final func withoutConstraints() -> Self {
        self.removeConstraints(self.constraints)
        return self
    }

    public final func fit(_ inset: CGFloat) -> Self {
        return self.fit(UIEdgeInsets(top: inset, left: inset, bottom: -inset, right: -inset))
    }

    public final func fit(_ edges: UIEdgeInsets) -> Self {
        guard let superview = self.superview else { return self }

        self.removeConstraints(self.constraints)
        self.translatesAutoresizingMaskIntoConstraints = false

        self.topAnchor.constraint(equalTo: superview.topAnchor, constant: edges.top).isActive = true
        self.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: edges.right).isActive = true
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: edges.bottom).isActive = true
        self.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: edges.left).isActive = true

        return self
    }

    /// Adds constraints to center self in parent view.
    public final func centered() -> Self {
        return self.withoutConstraints().centerVertically().centerHorizontally()
    }

    public final func centerVertically(_ constant: CGFloat = 0, sides: UIEdgeInsets? = nil) -> Self {
        guard let superview = self.superview else { return self }

        self.translatesAutoresizingMaskIntoConstraints = false
        self.centerYAnchor.constraint(equalTo: superview.centerYAnchor, constant: constant).isActive = true
        if let sides = sides {
            self.topAnchor.constraint(equalTo: superview.topAnchor, constant: sides.top).isActive = true
            self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -sides.bottom).isActive = true
        }

        return self
    }

    public final func centerHorizontally(_ constant: CGFloat = 0, sides: UIEdgeInsets? = nil) -> Self {
        guard let superview = self.superview else { return self }

        self.translatesAutoresizingMaskIntoConstraints = false
        self.centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: constant).isActive = true
        if let sides = sides {
            self.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: sides.left).isActive = true
            self.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: -sides.right).isActive = true
        }

        return self
    }

    public final func vLayout(under view: UIView? = nil, above: UIView?, edges: UIEdgeInsets) -> Self {
        guard let superview = self.superview else { return self }

        self.translatesAutoresizingMaskIntoConstraints = false
        self.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: edges.left).isActive = true
        self.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: -edges.right).isActive = true

        if let underView = view {
            self.topAnchor.constraint(equalTo: underView.bottomAnchor, constant: edges.top).isActive = true
        } else {
            self.topAnchor.constraint(equalTo: superview.topAnchor, constant: edges.top).isActive = true
        }

        if let above = above {
            if above === superview {
                self.bottomAnchor.constraint(equalTo: above.bottomAnchor, constant: -edges.bottom).isActive = true
            } else {
                self.bottomAnchor.constraint(equalTo: above.topAnchor, constant: edges.bottom).isActive = true
            }
        }

        return self
    }

    public final func hLayout(left view: UIView?, right: UIView?, edges: UIEdgeInsets) -> Self {
        guard let superview = self.superview else { return self }

        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: superview.topAnchor, constant: edges.top).isActive = true
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -edges.bottom).isActive = true

        if let leftView = view {
            self.leftAnchor.constraint(equalTo: leftView.rightAnchor, constant: edges.left).isActive = true
        } else {
            self.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: edges.left).isActive = true
        }

        if let right = right {
            if right === superview {
                self.rightAnchor.constraint(equalTo: right.rightAnchor, constant: -edges.right).isActive = true
            } else {
                self.rightAnchor.constraint(equalTo: right.leftAnchor, constant: edges.right).isActive = true
            }
        }

        return self
    }
}

//MARK: - STYLING

extension UIView {
    public final func background(_ color: UIColor?) -> Self {
        self.backgroundColor = color
        return self
    }

    public final func bordered(_ borderColor: UIColor, _ borderWidth: CGFloat = 1,
                               _ corners: CACornerMask = [.layerMinXMinYCorner, .layerMaxXMinYCorner,
                                                          .layerMinXMaxYCorner, .layerMaxXMaxYCorner]) -> Self {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.maskedCorners = corners

        return self
    }
}

