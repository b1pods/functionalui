//
//  UILabel+Functional.swift
//  FunctionalUI
//
//  Created by Dmitry Shelonin on 14/07/2019.
//  Copyright © 2019 Dmitry Shelonin. All rights reserved.
//

import UIKit

extension UILabel {
    public final func set(_ text: String?) -> Self {
        self.text = text
        return self
    }

    public final func set(_ textColor: UIColor) -> Self {
        self.textColor = textColor
        return self
    }

    public final func set(_ font: UIFont) -> Self {
        self.font = font
        return self
    }

    public final func set(_ textAlignment: NSTextAlignment) -> Self {
        self.textAlignment = textAlignment
        return self
    }

    public final func set(_ lineBrakMode: NSLineBreakMode) -> Self {
        self.lineBreakMode = lineBrakMode
        return self
    }

    public final func set(_ attributedText: NSAttributedString?) -> Self {
        self.attributedText = attributedText
        return self
    }

    public final func set(lines: Int) -> Self {
        self.numberOfLines = lines
        return self
    }
}
