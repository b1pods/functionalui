//
//  UITableView+Functional.swift
//  FunctionalUI
//
//  Created by Dmitry Shelonin on 15/07/2019.
//  Copyright © 2019 Dmitry Shelonin. All rights reserved.
//

import UIKit

extension UITableViewCell {
    public static var identifier: String { return String(describing: self) }
}

extension UITableView {
    public final func register(_ cell: AnyClass) -> Self {
        self.register(cell, forCellReuseIdentifier: String(describing: cell))
        return self
    }
}
